(* A simple compiler from expression to stack machine instructions. An example from Cpdt *)

(* First we define the operators *)
Require Import List.
Open Scope list_scope.

(* The two binary operators that we suport *)
Inductive binop := Plus | Times.

(* The source language is the language of expression *)

Inductive expr :=
| Const : nat   -> expr
| Binop : binop -> expr -> expr -> expr.

(* The machine code *)

Inductive instr :=
| push : nat -> instr
| exec : binop -> instr.

Definition program := list instr.




(* Our compiler *)
Fixpoint compile (e : expr) : program :=
  match e with
    | Const n         => push n :: nil
    | Binop op e1 e2  => compile e2 ++ compile e1 ++ exec op :: nil
  end.



(* The meaning of stuff *)

Definition binopDenote (op : binop) : nat -> nat -> nat :=
  match op with
    | Plus  => plus
    | Times => mult
  end.

Fixpoint expDenote (e : expr) : nat :=
  match e with
    | Const n         => n
    | Binop op e1 e2  => binopDenote op (expDenote e1) (expDenote e2)
  end.


(* Now for the definition of the stack machine *)

Definition stack := list nat.


(* Meaning of stack machine *)

Definition instrDenote (i : instr)(s : stack) : option stack :=
  match i, s with
    | push n, _               => Some (n :: s)
    | exec op, (x :: y :: sp) => Some ((binopDenote op x y) :: sp)
    | _,_                       => None
  end.

Fixpoint programDenote (p : program) (s : stack) : option stack :=
  match p with
    | nil          => Some s
    | (i :: rest)
      => match instrDenote i s with
           | Some sp => programDenote rest sp
           | _       => None
         end
  end.

Ltac unfold_fold f := unfold f; fold f.



(* The theorem we want to prove *)
Ltac simplifier :=
  repeat (unfold_fold compile); repeat (unfold_fold expDenote); simpl; autorewrite with core; simpl; trivial.

Ltac induct_on_expr :=
  let e := fresh "e" in intro e; induction e.


Ltac crush :=
  try repeat  match goal with
            | [ H : ?T |- ?T ]             => exact H
            | [ |- forall _: expr, _ ]     => induct_on_expr
            | [ |- context[(_ ++ _) ++ _]] => rewrite app_assoc_reverse
            | [ H : _ |- _ ]               => rewrite H
            | _ => intros; simplifier

         end.

(*
  repeat match goal with
           |[ H : ?T |- ?T ] => exact H
           | _ => repeat unfold_fold compile; repeate; autorewrite with core; simpl; trivial
         end.
*)

#[local] Hint Rewrite app_assoc_reverse.

Lemma compiler_is_better : forall e p s, programDenote (compile e ++ p) s  = programDenote p (expDenote e :: s).
Proof.


  (* The manual proof is here *)
  (*
  intro e.
  induction e.
  simpl. trivial.
  intros p s.
  unfold compile; fold compile.
  unfold expDenote; fold expDenote.
  SearchRewrite ((_++_) ++ _).
  rewrite app_assoc_reverse.

  rewrite IHe2.
  rewrite app_assoc_reverse.

  rewrite IHe1.
  simpl.
  trivial.

  Show Proof.
*)
  crush.
  
Qed.


Hint Rewrite compiler_is_better.

Theorem compiler_is_correct : forall (e : expr), programDenote (compile e) nil = Some (expDenote e :: nil).
Proof.
 (* Set Ltac Debug.*)
  crush.
  Show Proof.
Qed.
