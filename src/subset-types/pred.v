Lemma zgtz : 0 > 0 -> False.
Proof.
  intro x. inversion x.
Qed.

Extraction Language Haskell.


Definition pred (n : nat) : n > 0 -> nat :=
  match n with
    | 0 => fun pf : (0 > 0) => match zgtz pf with end
    | S m => fun _ => m
  end.

Extraction pred.

Definition Positives := { x : nat | x > 0 }.


Definition predSet (p : Positives) : nat :=
  match p with
    | exist _ 0 pf    => match zgtz pf with end
    | exist _ (S m) _ => m
  end.

Extraction predSet.

Definition predP : forall n : nat,  n > 0 -> { m | n = S m }.
Proof.
  refine (fun n =>
            match n with
              | 0   => fun pf => match zgtz pf with end
              | S m => fun _  => exist _ m _
            end).
  trivial.
Qed.



Definition predStrong (n : nat) : {m | n = S m} + {n = 0}.
  refine ( match n with
             | 0   => inright _
             | S m => inleft _
           end
         ); trivial; exists m; trivial.
Defined.

Print sumor.

Extract Inductive sumor => "Maybe" [ "Just" "Nothing" ].
Extraction predStrong.
