(*  This module explains the decidable equality *)


Ltac crush :=
  repeat trivial.


Definition Dec (A : Prop) : Set := {A} + {not A}.

Notation "'Yes'"        :=  (left _ _).
Notation "'No'"         :=  (right _ _).
Notation "'Reduce' x"   :=  (if x then Yes else No) (at level 50).

Ltac crush_inequality :=
  match goal with
    | [ |- S _ <> 0   ] => let H := fresh "H" in intro H; inversion H
  end.

Ltac nat_induction :=
  match goal with
    | [ |- forall n : nat, _ ] => let n := fresh "n" in intro n; induction n
  end.
  

Ltac crushit :=
  repeat match goal with
           | [ |- _ <> _ ] => crush_inequality
           | [ |- forall _ : nat,  _ ] => nat_induction
           | _  => trivial; auto
         end.
 

Lemma zneSn : forall n : nat, S n <> 0.
Proof.
  crushit.
Qed.

Fixpoint eq_nat_dec (n m : nat): Dec (n = m).
  refine (match n, m with
            | 0, 0     => Yes
            | S a, S b => Reduce (eq_nat_dec a b)
            | _,_      => No
          end); crushit.
Defined.

Require Import Extraction.
Extraction Language Haskell.
Extract Inductive sumbool => "Bool" [ "True"  "False" ].
Extraction eq_nat_dec.
