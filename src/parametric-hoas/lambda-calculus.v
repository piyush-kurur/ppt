(* 

Higher order abstract syntax is a way of encoding language level
bindings making use of the implementation language's (in our case
coq's) function abstraction.

This is described to some extent in chapter 17 of cpdt.

 *)

Require Import String.

Open Scope string.

Check "foo".

        

Inductive lambda : Set :=
| LV   : string -> lambda
| LAp  : lambda -> lambda -> lambda
| LAbs : string -> lambda -> lambda
.



Fixpoint subst (me  : string)(In : lambda) : lambda -> lambda :=
  let noSub := fun _ =>  In in
  let doSub := fun x =>  x   in
  match In with
    | LV   you       => if string_dec me you then doSub else noSub
    | LAp  f   e     => fun By => LAp (subst me f By) (subst me e By)
    | LAbs you body  => if string_dec me you then noSub else fun By => LAbs you (subst me body By)
  end.
   

Compute subst "x" (LV "x") (LV "t").
           

Section PHAOS.
  Variable var   : Type.

  Inductive term : Type :=
  | Var : var -> term 
  | App : term  -> term -> term
  | Abs : (var -> term) -> term.

End PHAOS.

Arguments Var [var] _ .
Arguments App [var] _ _ .
Arguments Abs [var] _ .

Fixpoint squash {v : Type} (t : term (term v)) : term v :=
  match t with
    | Var v   => v
    | App f e => App (squash f) (squash e)
    | Abs f   => Abs (fun v => squash (f (Var v)))
  end.



(* Closed terms abstract over their variable type *)
Definition Term := forall var, term var.

Definition Substitution :=  forall var, var -> term var.

Definition substitute {var : Type}(S : Substitution) (t : Term) : Term :=
  fun var => squash (S (term var) (t var) ).

  
                                                                           
                                                                           
Definition  omega : Term := fun _ => App (Abs (fun x => App (Var x) (Var x))) (Abs (fun x => App (Var x) (Var x))).
Definition iden   : Term := fun _ => Abs (fun x => Var x).
Eval compute in substitute (fun _ x => (Var x)) iden.
