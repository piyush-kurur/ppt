module Lambda where
import Data.String

-- | Abstract syntax
data L = V String
       | Ap L      L
       | Ab String L


-- | Higher order abstract syntax
data LH = FV  String
        | ApH  LH    LH
        | AbH (LH -> LH)


syntax :: LH -> L
hoas   :: L -> LH



-- | Free variable substitution in L.
subs :: String -> L -> L -> L
subs x var@(V y)  u   | x == y    = u
                      | otherwise = var
                                        
subs x (Ap f e)     u             = Ap (subs x f u) (subs x e u)
subs x fun@(Ab y e) u | x == y    = fun
                      | otherwise = Ab y (subs x e u)


----------------- Conversion to Syntax.

syntax = fst . syntaxP 0

syntaxP :: Int -> LH -> (L, Int)
syntaxP  l (FV s)    = (V s, l)
syntaxP  l (ApH f e) = (Ap fp ep, n)
  where (fp,m) = syntaxP l f
        (ep,n) = syntaxP m e
syntaxP l (AbH f) = (Ab (name l) body, m)
  where (body, m) = syntaxP (l+1) $ f $ fvar l
        name :: Int -> String
        name n = "?" ++ show n
        fvar :: Int -> LH
        fvar = FV . name

----------- Conversion of HOAS ---------------------------

hoas (V x)    = FV x
hoas (Ap f e) = ApH (hoas f)  (hoas e)
hoas (Ab x e) = AbH $ liftH  $ subs x e

liftH :: (L -> L) -> LH -> LH
liftH f  = hoas . f . syntax


-- | Do a reduction to whnf.
reduce :: LH -> LH
reduce x = case x of
            ApH f e -> app (reduce f) e
            _       -> x
  where app (AbH g) e = g e
        app r       e = ApH r e



paren :: Show a => a -> String
paren x = "(" ++ show x ++ ")"


instance Show L where
  show (V    x)  = x
  show (Ab x e)  = unwords ["fun", x , "=>", show e]
  show (Ap f e)  = unwords [func f, arg e]
    where func u@(Ab _ _) = paren u
          func u          = show u
          arg  a@(V _)    = show a
          arg  a          = paren a
 

instance IsString L where
  fromString = V


instance IsString LH where
  fromString = FV

instance Show LH where
  show = show . syntax
