% Course proposal: Programs, proofs and types
% Piyush P Kurur

# Introduction.

A software bug in your fancy mobile app is a mere annoyance; in
critical systems like railway signaling and flight control, it is
fatal. Engineering such critical software demands constructing
programs which are _guaranteed_ to meet its correctness
specifications. How does one build and engineer such software?
Register for this course and get an answer.


## Certified programming in practice.

The main theme of this course is _certified programs_, i.e. programs
that come with a _proof_ that it meets the desired _specifications_. A
specification is a property that the program has to satisfy for it to
be acceptable and we say that the program is correct if it executes
according to its specification. For example, a railway signaling
software should not set the signals such that it allows two trains to
be on a single line at the same time.

One important property of purported proof of correctness is that they
are _formal_ in the sense that they are checked _by the computer_ for
correctness. The proof of correctness of the program thus acts as a
_computer verifiable certificate_ for program correctness.

To write such certified programs, we need

1. A language to write the program in

2. A language to write the specifications

3. A mechanism to verify that the given program meets the necessary
   specification.


In this course, we study how to use the proof assistant [Coq] to build
such certified programs. The [Coq] proof assistant gives a unified way
to expression programs and specifications. Programs written for [Coq]
need to be of the correct "type" and the coq interpreter checks for
this. This type checking works as the formal mechanism for checking
the correctness of proofs.


## Who else would be interested ?

The underlying mechanism behind [Coq] proof assistant can also double
up in formulating and proving statements in mathematics --- for a
recent success see the complete proof of [Feit-Thompson theorem in
Coq][ftcoq]). Although we emphasis certified programs, what we learn
in this course is applicable to proving mathematical statements in
general and should be of interest to mathematicians and logicians as well

## Contents

The following topics will be covered in this course

1. [Coq] the proof assistant, (4-5 lectures)

2. Functional and dependent types programming in [Coq], 10-12 lectures

3. Proof tactics and proof automation. (10-12 lectures)

4. Proofs by reflection. (10-12 lectures)

## Pre-requisites.

We will start from the very basics so I have not put any official
pre-requisites. However here some aspects you might want to consider
when registering for this course.

1. An introduction to functional programming either as a course or
   through a course like Principles of Programming language can help
   you get started with a proof assistant.

2. This course should be considered as an _advanced course_ and hence
   plenty of mathematical maturity is expected.

3. Formal proofs are best done interactively using the
   computer. Therefore, you should also be comfortable installing
   [coq] and associated software on your machine. Typical GNU/Linux
   distribution comes with pre-packaged versions of coq, so this
   should not be too difficult a task.

## References

I plan to follow the book [Certified programming using dependent
types][cpdt], by [Adam Chlipala][adam] for this course.

All course related material (including the
[latest version of this course proposal][proposal]) is available at
<https://bitbucket.org/piyush-kurur/ppt>.


[cpdt]: <http://adam.chlipala.net/cpdt/> "Certified programming using dependent types"
[adam]: <http://adam.chlipala.net/> "Adam Chlipala"
[coq]: <https://coq.inria.fr/> "The coq proof assistant"
[haskell]: <https://www.haskell.org/> "Haskell programming language"
[ml]: <http://sml-family.org/> "Standard ML Family"
[ftcoq]: <http://www.msr-inria.fr/news/feit-thomson-proved-in-coq/>
[proposal]: <https://bitbucket.org/piyush-kurur/ppt/src/master/proposal/proposal.md>
