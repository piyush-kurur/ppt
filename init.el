;; Sample emacs configuration.
;;
;; Taken because the Debian/Ubuntu proof general package does not work as intended

;; From the proof general site.


(require 'package)
;; (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") ; see remark below
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)



;; Load company-coq when opening Coq files
(require 'proof-site)
(proofgeneral "coq")
(add-hook 'coq-mode-hook #'company-coq-mode) ; Can be ignored if you
					     ; do not want company coq

(setq auto-mode-alist
      (append auto-mode-alist
	      '(("\\.v$" . coq-mode))))
