Programs, proofs and types.
==========================

This repository contains source material corresponding to the course
titled Programs, Proofs and types that I am offering in the semester
June-Dec 2016.


[![About the course](https://img.youtube.com/vi/VqAtFZasvtc/hqdefault.jpg)](https://www.youtube.com/watch?v=VqAtFZasvtc)

Copying
-------

(C) 2016 Piyush P Kurur.

The material given below are released under the following license.

1. All notes, documentation, instructions etc are released under the
   Creative Commons Attribute Shared Alike license

2. All code samples, programs etc are released under the BSD3 license.

See the directory licenses for an exact terms and conditions of these
licenses.

I am happy to get contributions from you provided that

1. You agree to release the source under the licenses given above.

2. You agree to transfer the copyright to me.

In return, I pledge to keep the material under the same license.


[pg]:    <https://proofgeneral.github.io/> "Proof General Website"
